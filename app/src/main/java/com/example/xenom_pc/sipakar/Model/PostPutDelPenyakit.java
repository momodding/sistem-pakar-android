package com.example.xenom_pc.sipakar.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class PostPutDelPenyakit {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    Penyakit mPenyakit;
    @SerializedName("message")
    String message;

    public String getStatus(){
        return status;
    }

    public void setStatus(){
        this.status = status;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(){
        this.message = message;
    }

    public Penyakit getListDataPenyakit(){
        return mPenyakit;
    }

    public void setListDataPenyakit(Penyakit Penyakit){
        mPenyakit = Penyakit;
    }
}
