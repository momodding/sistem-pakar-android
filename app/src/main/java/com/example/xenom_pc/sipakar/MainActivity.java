package com.example.xenom_pc.sipakar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.xenom_pc.sipakar.Adapter.AdapterGejala;
import com.example.xenom_pc.sipakar.Model.Gejala;
import com.example.xenom_pc.sipakar.Model.GetGejala;
import com.example.xenom_pc.sipakar.Rest.ApiCLient;
import com.example.xenom_pc.sipakar.Rest.ApiInterface;

import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button btIns;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiCLient.getClient().create(ApiInterface.class);
        ma=this;
        refresh();
    }

    public void refresh() {
        Call<GetGejala> gejalaCall = mApiInterface.getGejala();
        gejalaCall.enqueue(new Callback<GetGejala>(){

            @Override
            public void onResponse(Call<GetGejala> call, Response<GetGejala> response) {
                if (response.isSuccessful()){
                    List<Gejala> GejalaList = response.body().getListDataGejala();
                    Log.d("Retrofit Get", "Jumlah data gejala: "+ String.valueOf(GejalaList.size()));
                    mAdapter = new AdapterGejala(GejalaList);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetGejala> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }

    public void doInsert(View view) {
        Intent insertAct = new Intent(this, InsertActivity.class);
        startActivity(insertAct);
    }
}
