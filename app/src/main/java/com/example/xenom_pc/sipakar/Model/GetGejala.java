package com.example.xenom_pc.sipakar.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class GetGejala {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Gejala> listDataGejala;
    @SerializedName("message")
    String message;

    public String getStatus(){
        return status;
    }

    public void setStatus(){
        this.status = status;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(){
        this.message = message;
    }

    public List<Gejala> getListDataGejala(){
        return listDataGejala;
    }

    public void setListDataGejala(List<Gejala> listDataGejala){
        this.listDataGejala = listDataGejala;
    }
}
