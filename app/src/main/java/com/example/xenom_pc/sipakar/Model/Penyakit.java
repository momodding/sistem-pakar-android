package com.example.xenom_pc.sipakar.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class Penyakit {
    @SerializedName("id_penyakit")
    private String id;
    @SerializedName("nama")
    private String nama;

    public Penyakit(){}

    public Penyakit(String id, String nama){
        this.id = id;
        this.nama = nama;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getPenyakit(){
        return nama;
    }

    public void setPenyakit(String nama){
        this.nama = nama;
    }
}
