package com.example.xenom_pc.sipakar.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class Gejala {
    @SerializedName("id_gejala")
    private String id;
    @SerializedName("gejala")
    private String gejala;

    public Gejala(){}

    public Gejala(String id, String gejala){
        this.id = id;
        this.gejala = gejala;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getGejala(){
        return gejala;
    }

    public void setGejala(String gejala){
        this.gejala = gejala;
    }
}
