package com.example.xenom_pc.sipakar.Rest;

import com.example.xenom_pc.sipakar.Model.GetGejala;
import com.example.xenom_pc.sipakar.Model.GetPenyakit;
import com.example.xenom_pc.sipakar.Model.PostPutDelGejala;
import com.example.xenom_pc.sipakar.Model.PostPutDelPenyakit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by xenom-pc on 12/22/17.
 */

public interface ApiInterface {
    @GET("gejala")
    Call<GetGejala> getGejala();
    @FormUrlEncoded
    @POST("gejala")
    Call<PostPutDelGejala> postGejala(@Field("id") String id,
                                      @Field("gejala") String gejala);
    @FormUrlEncoded
    @PUT("gejala")
    Call<PostPutDelGejala> putGejala(@Field("id") String id,
                                     @Field("gejala") String gejala);
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "gejala", hasBody = true)
    Call<PostPutDelGejala> deleteGejala(@Field("id") String id);

    @GET("penyakit")
    Call<GetPenyakit> getPenyakit();
    @FormUrlEncoded
    @POST("penyakit")
    Call<PostPutDelPenyakit> postPenyakit(@Field("id") String id,
                                        @Field("nama") String nama);
    @FormUrlEncoded
    @PUT("penyakit")
    Call<PostPutDelPenyakit> putPenyakit(@Field("id") String id,
                                     @Field("nama") String nama);
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "penyakit", hasBody = true)
    Call<PostPutDelPenyakit> deletePenyakit(@Field("id") String id);
}
