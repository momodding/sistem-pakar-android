package com.example.xenom_pc.sipakar.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.xenom_pc.sipakar.EditActivity;
import com.example.xenom_pc.sipakar.Model.Gejala;
import com.example.xenom_pc.sipakar.R;

import java.util.List;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class AdapterGejala extends RecyclerView.Adapter<AdapterGejala.MyViewHolder>{
    private List<Gejala> mGejalaList;

    public AdapterGejala(List<Gejala> GejalaList){
        mGejalaList = GejalaList;
    }

    @Override
    public AdapterGejala.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gejala_list, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(mView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterGejala.MyViewHolder holder, final int position) {
        holder.mTextViewId.setText("ID Gejala = " + mGejalaList.get(position).getId());
        holder.mTextViewGejala.setText("Gejala = " + mGejalaList.get(position).getGejala());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), EditActivity.class);
                mIntent.putExtra("id_gejala", mGejalaList.get(position).getId());
                mIntent.putExtra("gejala", mGejalaList.get(position).getGejala());
                view.getContext().startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGejalaList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextViewId, mTextViewGejala;

        public MyViewHolder(View itemView){
            super(itemView);
            mTextViewId = (TextView)itemView.findViewById(R.id.tvId);
            mTextViewGejala =(TextView)itemView.findViewById(R.id.tvGejala);
        }
    }
}
