package com.example.xenom_pc.sipakar.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.xenom_pc.sipakar.EditActivity;
import com.example.xenom_pc.sipakar.Model.Penyakit;
import com.example.xenom_pc.sipakar.R;

import java.util.List;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class AdapterPenyakit extends RecyclerView.Adapter<AdapterPenyakit.MyViewHolder>{
    private List<Penyakit> mPenyakitList;

    public AdapterPenyakit(List<Penyakit> PenyakitList){
        mPenyakitList = PenyakitList;
    }

    @Override
    public AdapterPenyakit.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gejala_list, parent, false);
        AdapterPenyakit.MyViewHolder myViewHolder = new AdapterPenyakit.MyViewHolder(mView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterPenyakit.MyViewHolder holder, final int position) {
        holder.mTextViewId.setText("ID Penyakit = " + mPenyakitList.get(position).getId());
        holder.mTextViewPenyakit.setText("Penyakit = " + mPenyakitList.get(position).getPenyakit());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), EditActivity.class);
                mIntent.putExtra("id_penyakit", mPenyakitList.get(position).getId());
                mIntent.putExtra("nama", mPenyakitList.get(position).getPenyakit());
                view.getContext().startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPenyakitList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextViewId, mTextViewPenyakit;

        public MyViewHolder(View itemView){
            super(itemView);
            mTextViewId = (TextView)itemView.findViewById(R.id.tvId);
            mTextViewPenyakit =(TextView)itemView.findViewById(R.id.tvGejala);
        }
    }
}
