package com.example.xenom_pc.sipakar.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class PostPutDelGejala {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    Gejala mGejala;
    @SerializedName("message")
    String message;

    public String getStatus(){
        return status;
    }

    public void setStatus(){
        this.status = status;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(){
        this.message = message;
    }

    public Gejala getListDataGejala(){
        return mGejala;
    }

    public void setListDataGejala(Gejala Gejala){
        mGejala = Gejala;
    }
}
