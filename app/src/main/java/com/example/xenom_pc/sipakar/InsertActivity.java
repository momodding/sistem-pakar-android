package com.example.xenom_pc.sipakar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xenom_pc.sipakar.Model.PostPutDelGejala;
import com.example.xenom_pc.sipakar.Rest.ApiCLient;
import com.example.xenom_pc.sipakar.Rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertActivity extends AppCompatActivity {

    EditText edtId, edtGejala;
    Button btnInsert;
    ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        edtId = (EditText)findViewById(R.id.edtId);
        edtGejala = (EditText)findViewById(R.id.edtGejala);
        mApiInterface = ApiCLient.getClient().create(ApiInterface.class);
        btnInsert = (Button)findViewById(R.id.btnInserting);
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<PostPutDelGejala> postGejalaCall = mApiInterface.postGejala(edtId.getText().toString(), edtGejala.getText().toString());
                postGejalaCall.enqueue(new Callback<PostPutDelGejala>() {
                    @Override
                    public void onResponse(Call<PostPutDelGejala> call, Response<PostPutDelGejala> response) {
                        MainActivity.ma.refresh();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<PostPutDelGejala> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    public void doBackGo(View view) {
        MainActivity.ma.refresh();
        finish();
    }
}
