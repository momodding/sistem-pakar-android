package com.example.xenom_pc.sipakar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xenom_pc.sipakar.Model.PostPutDelGejala;
import com.example.xenom_pc.sipakar.Rest.ApiCLient;
import com.example.xenom_pc.sipakar.Rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditActivity extends AppCompatActivity {

    EditText edtId, edtGejala;
    Button btnUpdate, btnDelete;
    ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        edtId = (EditText)findViewById(R.id.edtId);
        edtGejala = (EditText)findViewById(R.id.edtGejala);
        Intent mIntent = getIntent();
        edtId.setText(mIntent.getStringExtra("id_gejala"));
        edtId.setTag(edtId.getKeyListener());
        edtId.setKeyListener(null);
        edtGejala.setText(mIntent.getStringExtra("gejala"));
        mApiInterface = ApiCLient.getClient().create(ApiInterface.class);
        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<PostPutDelGejala> updateGejalaCall = mApiInterface.putGejala(edtId.getText().toString(),
                        edtGejala.getText().toString());
                updateGejalaCall.enqueue(new Callback<PostPutDelGejala>() {
                    @Override
                    public void onResponse(Call<PostPutDelGejala> call, Response<PostPutDelGejala> response) {
                        MainActivity.ma.refresh();
                        finish();
                    }

                    @Override
                    public void onFailure(Call<PostPutDelGejala> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        btnDelete = (Button)findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtId.getText().toString().trim().isEmpty()==false){
                    Call<PostPutDelGejala> deleteGejalaCall = mApiInterface.deleteGejala(edtId.getText().toString());
                    deleteGejalaCall.enqueue(new Callback<PostPutDelGejala>() {
                        @Override
                        public void onResponse(Call<PostPutDelGejala> call, Response<PostPutDelGejala> response) {
                            MainActivity.ma.refresh();
                            finish();
                        }

                        @Override
                        public void onFailure(Call<PostPutDelGejala> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Error ", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else {
                    Toast.makeText(getApplicationContext(), "Error ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void doBackGo(View view) {
        MainActivity.ma.refresh();
        finish();
    }
}
