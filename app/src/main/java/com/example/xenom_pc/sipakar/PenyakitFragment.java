package com.example.xenom_pc.sipakar;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xenom_pc.sipakar.Adapter.AdapterGejala;
import com.example.xenom_pc.sipakar.Adapter.AdapterPenyakit;
import com.example.xenom_pc.sipakar.Model.Gejala;
import com.example.xenom_pc.sipakar.Model.GetGejala;
import com.example.xenom_pc.sipakar.Model.GetPenyakit;
import com.example.xenom_pc.sipakar.Model.Penyakit;
import com.example.xenom_pc.sipakar.Rest.ApiCLient;
import com.example.xenom_pc.sipakar.Rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PenyakitFragment extends Fragment {

    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static PenyakitFragment sakit;


    public PenyakitFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_penyakit, container, false);
        mRecyclerView = (RecyclerView)fragmentView.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiCLient.getClient().create(ApiInterface.class);
        sakit=this;
        refresh();
        return fragmentView;
    }

    private void refresh() {
        Call<GetPenyakit> penyakitCall = mApiInterface.getPenyakit();
        penyakitCall.enqueue(new Callback<GetPenyakit>(){

            @Override
            public void onResponse(Call<GetPenyakit> call, Response<GetPenyakit> response) {
                if (response.isSuccessful()){
                    List<Penyakit> PenyakitList = response.body().getListDataPenyakit();
                    Log.d("Retrofit Get", "Jumlah data gejala: "+ String.valueOf(PenyakitList.size()));
                    mAdapter = new AdapterPenyakit(PenyakitList);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetPenyakit> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }


}
