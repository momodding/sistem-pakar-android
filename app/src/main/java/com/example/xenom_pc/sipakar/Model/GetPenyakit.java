package com.example.xenom_pc.sipakar.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by xenom-pc on 12/22/17.
 */

public class GetPenyakit {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Penyakit> listDataPenyakit;
    @SerializedName("message")
    String message;

    public String getStatus(){
        return status;
    }

    public void setStatus(){
        this.status = status;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(){
        this.message = message;
    }

    public List<Penyakit> getListDataPenyakit(){
        return listDataPenyakit;
    }

    public void setListDataGejala(List<Penyakit> listDataPenyakit){
        this.listDataPenyakit = listDataPenyakit;
    }
}
